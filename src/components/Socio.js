import React, { Component } from 'react';
import '../assets/css/Socio.css'
import Item from "./Item";

export default class Socio extends Component {

	constructor(props){
		super(props);
		this.socio = props.socio;
		this.buttons = [
            {
                name: 'Modificar',
                action: 'modificacion',
                function: ()=>{console.error('modificacion')}
            },
            {
                name: 'Adherir',
                action: 'alta',
                function: ()=>{console.error('alta')}
            },
            {
                name: 'Dar de baja',
                action: 'baja',
                function: ()=>{console.error('baja')}
            }
        ]
	}

	GetContent()
	{
		return (
			<div className='socio-container'>
				<div className='info'>
					<div className='property'>
						<div className='property-title'>DNI:</div>
						<div className='property-value'>{this.socio.dni}</div>
					</div>
					<div className='property'>
						<div className='property-title'>Telefono:</div>
						<div className='property-value'>{this.socio.phone}</div>
					</div>
					<div className='property'>
						<div className='property-title'>Mail:</div>
						<div className='property-value'>{this.socio.email}</div>
					</div>
				</div>
				<div className='info'>
					<div className='property'>
						<div className='property-title'>Suscripción:</div>
						<div className='property-value'>{this.socio.subscription}</div>
					</div>
					<div className='property'>
						<div className='property-title'>Apto medico:</div>
						<div className='property-value'>{this.socio.healthDataState}</div>
					</div>
					<div className='property'>
						<div className='property-title'>Dirección:</div>
						<div className='property-value'>{this.socio.address}</div>
					</div>
				</div>
				
				<div className='info'>
					<div className='property'>
						<div className='property-title'>Descripción Médica:</div>
						<div className='property-value'>{this.socio.healthData}</div>
					</div>
				</div>
			</div>
		);
	}

	render()
	{
		return (
			<Item content={this.GetContent()} buttons={this.buttons} title={this.socio.name + ' ' + this.socio.surname}/>
		);
	}
}