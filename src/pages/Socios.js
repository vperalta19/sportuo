import React, { Component } from 'react'
import Socio from '../components/Socio';

export default class Socios extends Component {
	

	render() {
		const socio = {
			name: 'Juan Manuel',
			surname: 'Belgrano',
			dni: 42889052,
			phone: 5146584321684,
			email: 'jmbelgrano@uade.edu.ar',
			address: 'Libertad 269',
			healthDataState: 'VIGENTE',
			subscription: 'Mensual',
			healthData: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris orci nisl, varius et sem in, bibendum faucibus orci. Duis tempor tortor nisl, sit amet maximus ligula tempus eu. Nullam id condimentum felis, vel molestie orci. Phasellus non ullamcorper turpis. Nullam luctus, nunc a iaculis sollicitudin, leo felis posuere ex, ac cursus massa sapien rhoncus mi. '
		}
		return (
			<Socio socio={socio}/>
		)
	}
}
